import { combineReducers } from "redux";

import provider from "./provider";

export default combineReducers({
    provider
});